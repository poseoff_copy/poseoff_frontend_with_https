import {
    useQuery,
} from '@tanstack/react-query'

import {
    postApiUrl2,
} from "../../service/service/Service";
import { apiPaths } from '../../service/apipaths/ApiPaths';
export default function usePlayerName(onSuccess=undefined,onError=undefined) {
    return useQuery({
        queryKey: ['getPlayeName'],
        queryFn: () => postApiUrl2({}, apiPaths.getPlayerName, "application/json"),
        refetchOnWindowFocus:false,
    })
}