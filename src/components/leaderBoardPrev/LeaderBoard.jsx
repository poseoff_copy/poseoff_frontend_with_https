import React, { useState, useContext, useEffect } from "react";
import "./leaderboardcss.css";
import ResultHeader from "../../common/header/ResultHeader";
import { mainContext } from "../../store/Store";
import { getApiUrl2, postApiGcp } from "../../service/service/Service";
import { apiPaths } from "../../service/apipaths/ApiPaths";
import { useNavigate } from "react-router-dom";
import ProgressLoader from "../../common/ProgressLoader";

const LeaderBoard = () => {
  const navigate = useNavigate();
  const { userInfo } = useContext(mainContext);
  const [poseData, setPoseData] = useState([]);
  const [userRank, setUserRank] = useState(null);
  const [username, setUserName] = useState(null);
  const [usertime, setUsertime] = useState();

  const leaderboardFunc = () => {
    navigate("/LeaderBoardRanking");
  }

  const handleClose = () => {
    navigate("/EventPhoto");
  }

  useEffect(() => {
    getBoadrdData()
  }, []);

  function getBoadrdData() {
    getApiUrl2(apiPaths.getUserScore + '?tx_id=' + userInfo.txtId)
      .then((res) => {
        console.log(res.data);
        const data = res.data[0];
        const cusPosData = [
          { sendText: "pose_1", text: 'Pose 1', time: data.pose_1 },
          { sendText: "pose_2", text: 'Pose 2', time: data.pose_2 },
          { sendText: "pose_3", text: 'Pose 3', time: data.pose_3 },
          { sendText: "pose_4", text: 'Pose 4', time: data.pose_4 },
          { sendText: "pose_5", text: 'Pose 5', time: data.pose_5 },
          { sendText: "pose_6", text: 'Pose 6', time: data.pose_6 },
          { sendText: "pose_7", text: 'Pose 7', time: data.pose_7 },
          { sendText: "pose_8", text: 'Pose 8', time: data.pose_8 },
          { sendText: "pose_9", text: 'Pose 9', time: data.pose_8 },
          { sendText: "pose_10", text: 'Pose 10', time: data.pose_10 },
        ]
        setPoseData(cusPosData);
        setUserRank(data["your_position"]);
        setUserName(data["username"]);
      })
      .catch((err) => {

      })
  }

  return (
    <>
      <ResultHeader handleClose={handleClose} />
      <div className="content_container">
        <div className="bravo">Bravo, {username}!</div>
        {userRank <= 5 ? <div className="ranking">You are in top {userRank}!</div>
          : <div className="ranking">You finished the game on rank {userRank}</div>}
      </div>
      <div className="alpha_container">
        <div className="main_content_container">
          <div className="posetime_txt">
            POSE-OFF TIME
          </div>
          <div className="pose_time_container_leaderboard">
            {poseData.map((item,i) =>
              <div key={i} className="pose_time_box_leaderboard" >
                <div style={{
                  backgroundColor: item.time == "" ? "#EC4058" : '#2ED573',
                  color: item.time == "" ? "#9E9E9E" : "#FFFFFF",
                }}
                >
                  {item.time != "" && `${item.time}s`}
                </div>
                <div>{item.text}</div>
              </div>
            )}
          </div>
        </div>
      </div>

      <div className="leader__board-button-container" onClick={leaderboardFunc}>
        <div className="">
          <div>LEADERBOARD</div>
          <div className="loader_box">
            <ProgressLoader time={10} onFinish={leaderboardFunc}/>
          </div>
        </div>
      </div>

    </>
  );
};

export default LeaderBoard;
