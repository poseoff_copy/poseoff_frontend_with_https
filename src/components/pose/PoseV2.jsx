import React, { useRef, useEffect, useState,useContext } from "react";
import Webcam from "react-webcam";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import { mainContext } from "../../store/Store";

import { postApi, postApiGcp, cloudpostApi } from "../../service/service/Service";
import { apiPaths } from "../../service/apipaths/ApiPaths";
import "./Pose.css";
import "../../common/cameraComponent/cameracomp.css";
// import superManIst from "../../assets/superman/supermanst.png";
import user from "../../assets/user/1702026954787.png";
import CameraComponent from "../../common/cameraComponent/cameraComponent";
import Header from "../../common/header/Header";
import countDownImg from "../../assets/countdown.svg";
import Pose1 from "../../assets/updatedPose/00cd3c48fab04fd4ab9fd8fc042eff94.jpg";
import Pose2 from "../../assets/Pose2.jpg";
import Pose3 from "../../assets/Pose3.png";
import Pose4 from "../../assets/updatedPose/a-confidant-superhero-posing-for-camera-an-action-pose-masterpiece-realistic-folded-arms-nikon-33435838.png";
import Pose5 from "../../assets/supermansec.png";
// import Pose1 from "../../assets/Pose1.png";
import yesImg from "../../assets/Yes.svg";
import { useNavigate } from "react-router-dom";

const Pose = (props) => {
  const [showImageNumber, setShowImageNumber] = useState({
   
    1: Pose1,
    2: Pose2,
    3: Pose3,
    4: Pose4,
    5: Pose5,
  });
  const [count, setCount] = useState(1);
  const [cloudPoseValue, setCloudPoseValue] = useState(false);
  const {userDetail} = useContext(mainContext);
  const [showReadyToPose, setReadyToPose] = useState(false);
  const webcamRef = useRef(null);
  const [detection, setDetection] = useState(false);
  const [timerKey, setTimerKey] = useState(0);
  const [remainingTime, setRemainingTime] = useState(0);
  const [gameTime, setGameTime] = useState(0);
  const [gameTimeCloud, setGameTimeloud] = useState(0);
  const userFormData = useRef();
  console.log(userFormData,'userFormData')
  const navigate = useNavigate(); 
  // let edgeBool = false;
  // let cloudBool = false;

  const detectionStatus = useRef({
    edge:false,
    cloud:false,
  })

  const timeCompleteHandler = async (apiStatus) => {
    setReadyToPose(true);
    console.log('timer',  gameTimeCloud, gameTime)

  //   if (detectionStatus.current.cloud){
  //     detectionStatus.current.cloud=false;
  //     const response = await postApiGcp({
  //       Id:userDetail?._id,
  //       cloud_score: gameTimeCloud,
  //       edge_score: 15,
  //       pose: `Pose${count}`,
  //   }, apiPaths?.userScore);
  // }
  //   if (detectionStatus.current.edge){
  //     detectionStatus.current.edge=false;
  //     const response = await postApiGcp({
  //       Id:userDetail?._id,
  //       cloud_score:15,
  //       edge_score: gameTime,
  //       pose: `Pose${count}`,
  //   }, apiPaths?.userScore);
  //   }
    detectionStatus.current.cloud=false;
    detectionStatus.current.edge=false;
    // const response1 = await postApiGcp(userFormData.current, apiPaths?.savePictures);
      const response = await postApiGcp({
          Id:userDetail?._id,
          cloud_score: gameTimeCloud,
          edge_score: gameTime,
          pose: `Pose${count}`,
      }, apiPaths?.userScore);
    setGameTime(0)
    setGameTimeloud(0)
    if(apiStatus){
      setTimeout(() => {
        setReadyToPose(false);
       
        setCount((prev) => {
          if (prev === 5) {
            // return 1;
            navigate('/LeaderBoard');
          }
          return prev + 1;
        });
        setTimerKey((prev) => prev + 1);
      }, 1000);
    }
    

    // return { shouldRepeat: true, delay: 4 };
  };


  // const getUserpose= async () => {
  //   const response = await getApi(apiPaths?.playingPoseOff);
  //   // response?.data?.map((item) => {
  //   //   if (item?.status === "playing") {
  //   //     setUserDetail(item);
  //   //     navigate("/WelcomePage");
  //   //   }
  //   // });
  // };

  const renderTime = ({ remainingTime }) => {
    setRemainingTime(remainingTime);
    return (
      <div className="timer">
        <div className="value">{remainingTime}s</div>
      </div>
    );
  };

  const capture = async () => {
    const imageSrc = webcamRef.current.getScreenshot();
    const formData = new FormData();
    formData.append("filedata", imageSrc);
    formData.append("pose_name", `Pose${count}`);
    userFormData.current = formData

    if(!detectionStatus.current.edge){
      const response = await postApi(formData, apiPaths?.upload_pose);
      setDetection(response?.data?.detection);
      if (response?.data?.detection) {

        setGameTime(15 - remainingTime);
        detectionStatus.current.edge = true;
        const response1 = await postApiGcp(formData, apiPaths?.savePictures);
        // timeCompleteHandler(false);
      }
    }

    if(!detectionStatus.current.cloud){
      const responseFromCloud = await cloudpostApi(formData, apiPaths?.upload_pose);
      setCloudPoseValue(responseFromCloud?.data?.detection);
      if(responseFromCloud?.data?.detection){
        
        setGameTimeloud(15 - remainingTime);
        detectionStatus.current.cloud = true;
      }
    }

    
    if(detectionStatus.current.cloud && detectionStatus.current.edge){
      const sendData = {
        Id:userDetail?._id,
        cloud_score: gameTimeCloud,
        edge_score: gameTime,
        pose: `Pose${count}`,
      };
      const response = await postApiGcp(sendData, apiPaths?.userScore);
      
      timeCompleteHandler(false);
      setTimeout(() => {
        setReadyToPose(false);
       
        setCount((prev) => {
          if (prev === 5) {
            // return 1;
            navigate('/LeaderBoard');
          }
          return prev + 1;
        });
        setTimerKey((prev) => prev + 1);
      }, 1000);
    }
  };

  useEffect(() => {
    capture();

    // const id = setInterval(capture, 1000);
    // return () => clearInterval(id);
  }, [count, remainingTime]);

  return (
    <>
      <Header />
      <div className="pose__parent-container">
        <div className="poses__img-container">
          <div className="poses__number-container">Pose {count} of 5</div>
          <div className="pose_img-left">
            <img
              className="poses__img-pose"
              src={showImageNumber[count]}
              alt=""
            />
          </div>
          <span className="copy__the-pose-text">Copy the pose</span>
          {showReadyToPose && (
            <img className="count__down-img img-fluid" src={countDownImg} />
          )}
          {showReadyToPose && (
            <div className="get__ready-text-container">
              Get Ready for <br />
              new pose
            </div>
          )}
          <div className="countdown__time-container">
            <CountdownCircleTimer
              isPlaying
              key={timerKey}
              duration={15}
              colors={["#004777", "#F7B801", "#A30000", "#A30000"]}
              colorsTime={[7, 5, 2, 0]}
              size={60}
              strokeWidth={5}
              onComplete={detectionStatus.current.cloud || detectionStatus.current.edge ? ()=>timeCompleteHandler(false): timeCompleteHandler}
              // onComplete={timeCompleteHandler}
              // onComplete={() => ({ shouldRepeat: true, delay: 1 })}
            >
              {/* {({ remainingTime }) => remainingTime} */}
              {renderTime}
            </CountdownCircleTimer>
          </div>
        </div>
        <div className="poses__img-container">
          <div className="poses__img-right">
            {/* <img
            className="poses__img__camera img-fluid"
            src={user}
            alt=""
            srcset=""
          /> */}
            {/* <CameraComponent /> */}
            {/* {detection && alert("your pose is right")} */}
            <Webcam
              audio={false}
              // height={160}
              ref={webcamRef}
              screenshotFormat="image/jpeg"
              // width={160}
              // width={100}
            />
          </div>
          <div className="d-flex video__bottom-container">
            <div
              className={`poseoff__perfomance__container left__container ${
                detection && `bg-green`
              }`}
            >
              <div className="d-flex flex-column">
                <span className="poses__score-perfomance">Performance on:</span>
                <span className="poses__score-perfomance-game_name">
                  Cloud Edge
                </span>
              </div>
              <div className="d-flex align-items-center">
                <span className="poses__game-time">
                  {/* {gameTime} */} 
                  {detection ? `00:${gameTime}s` : `00:00S`}
                </span>
                {detection && (
                  <span className="yes__img">
                    <img src={yesImg} />
                  </span>
                )}
              </div>
            </div>
            <div className={`poseoff__perfomance__container ${
              cloudPoseValue && `bg-green`
            }`} >
              <div className="d-flex flex-column">
                <span className="poses__score-perfomance">Performance on:</span>
                <span className="poses__score-perfomance-game_name">
                  Google Cloud
                </span>
              </div>
              <div>
                <span className="poses__game-time">{detection ? `00:${gameTimeCloud}s` : `00:00S`}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Pose;
