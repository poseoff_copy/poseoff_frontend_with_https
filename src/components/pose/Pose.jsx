import React, { useRef, useEffect, useState, useContext } from "react";
import Webcam from "react-webcam";
// import {
//   useQuery,
// } from '@tanstack/react-query'
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import ReactPlayer from 'react-player';
import { mainContext } from "../../store/Store";
import {
  postApi,
  postApiGcp,
  cloudpostApi,
  postApiUrl2,
  getApiUrl2,
} from "../../service/service/Service";
import { apiPaths } from "../../service/apipaths/ApiPaths";
import "./Pose.css";
import "../../common/cameraComponent/cameracomp.css";
import Header from "../../common/header/Header";
import countDownImg from "../../assets/countdown.svg";
import Pose1 from "../../assets/updatedPose/pose1.jpg";
import Pose2 from "../../assets/updatedPose/Pose2Updated.png";
import Pose3 from "../../assets/updatedPose/Pose3Updated.png";
import Pose4 from "../../assets/updatedPose/foldingArms.png";
import Pose5 from "../../assets/updatedPose/Pose5Updated.png";
import Pose6 from "../../assets/updatedPose/newPose6.jpg";
import Pose7 from "../../assets/updatedPose/pose7.jpg";
import Pose8 from "../../assets/updatedPose/pose8.jpg";
import Pose9 from "../../assets/updatedPose/pose9.jpg";
import Pose10 from "../../assets/updatedPose/pose10.jpg";
import { useFetcher, useNavigate } from "react-router-dom";
// import usePlayerName from "../../hooks/usePlayerName/usePlayerName";

const Pose = (props) => {
  const { spinnerInfo, setUserInfo, anonymousUserInfo, anonymousUserId, userImage, envPlatform, camUrl, setCamUrl, camMode } = useContext(mainContext);
  const navigate = useNavigate();
  const timeData = useRef({
    remainingTime: 60,
    timeReached: 60,
  });
  const poseData = useRef([
    { name: "Pose1", img: Pose1 },
    { name: "Pose2", img: Pose2 },
    { name: "Pose3", img: Pose3 },
    { name: "Pose4", img: Pose4 },
    { name: "Pose5", img: Pose5 },
    { name: "Pose6", img: Pose6 },
    { name: "Pose7", img: Pose7 },
    { name: "Pose8", img: Pose8 },
    { name: "Pose9", img: Pose9 },
    { name: "Pose10", img: Pose10 },
  ]).current;
  const [currentPosNum, setCurrentPoseNum] = useState(0);
  const [isPending, setIsPending] = useState(false);
  const facingMode = camMode;



  const webcamRef = React.useRef(null);
  const [poseTimes, setPoseTime] = useState([
    { sendText: "p1_score", text: 'Pose 1', time: 0, isDisabled: true, skiped: false },
    { sendText: "p2_score", text: 'Pose 2', time: 0, isDisabled: true, skiped: false },
    { sendText: "p3_score", text: 'Pose 3', time: 0, isDisabled: true, skiped: false },
    { sendText: "p4_score", text: 'Pose 4', time: 0, isDisabled: true, skiped: false },
    { sendText: "p5_score", text: 'Pose 5', time: 0, isDisabled: true, skiped: false },
    { sendText: "p6_score", text: 'Pose 6', time: 0, isDisabled: true, skiped: false },
    { sendText: "p7_score", text: 'Pose 7', time: 0, isDisabled: true, skiped: false },
    { sendText: "p8_score", text: 'Pose 8', time: 0, isDisabled: true, skiped: false },
    { sendText: "p9_score", text: 'Pose 9', time: 0, isDisabled: true, skiped: false },
    { sendText: "p10_score", text: 'Pose 10', time: 0, isDisabled: true, skiped: false },
  ]);
  const [showLogo, setShowLogo] = useState(false);
  const [showPopup, setShowPopup] = useState(false);
  const playerRef = useRef(null);
  const canvasRef = useRef(null);
  const [screenshot, setScreenshot] = useState(null);
  // const [username, setUserName] = useState({
  //   username: undefined,
  //   tID: undefined
  // });
  const sendRequest = useRef(true);
  useEffect(() => {
    if (!showLogo) {
      sendRequest.current = true;
    }
  }, [showLogo])

  useEffect(() => {
    if (playerRef.current != null) {
      let player = playerRef.current;
      // player.crossOrigin="anonymous"
      player.src = camUrl;
      console.log("qq :", camMode);
    }

    // player.play();
  }, [])

  // const getSettings = async () => {
  //   const response = await getApiUrl2(apiPaths?.getSettings);
  //   const data = response?.data[0];
  //   setCamUrl(data.cam_url);
  // }

  // useEffect(()=>{
  //   getSettings();
  // }, [])

  // const { data: playerData } = usePlayerName()
  // useEffect(()=>{
  //   getPlayerName();
  // }, [])
  // const username = {
  //   username: playerData?.data?.username,
  //   tID: playerData?.data?._id
  // }

  // const getPlayerName = () => {
  //   console.log("qq :",apiPaths.getPlayerName);
  //   const sendData = {
  //   };
  //   postApiUrl2(sendData, apiPaths.getPlayerName, "application/json")
  //   .then((res) => {
  //     setUserName({
  //       username: res.data.username,
  //       tID: res.data._id
  //     });
  //   })
  //   .catch((err) => {
  //     console.log('error');
  //   })
  // }

  // const cameraViewHandler = () => {
  //   setFacingMode(
  //     prevState =>
  //       prevState === FACING_MODE_USER
  //         ? FACING_MODE_ENVIRONMENT
  //         : FACING_MODE_ENVIRONMENT
  //   );
  // };

  async function setPostData(remainingTime) {
    timeData.current.remainingTime = remainingTime;
    let poseImg;
    if (showPopup) return;
    if (!sendRequest.current) return;
    if (camMode != "rtsp") {
      if (webcamRef.current == null) return;
      poseImg = webcamRef.current.getScreenshot();
    }
    else {
      // const player = playerRef.current.getInternalPlayer();
      const player = playerRef.current;
      // player.crossOrigin = "anonymous";
      const canvas = canvasRef.current;


      // Ensure player and canvas exist
      if (!player || !canvas) return;

      const ctx = canvas.getContext('2d');
      ctx.drawImage(player, 0, 0, canvas.width, canvas.height);

      // Convert canvas to image and set as screenshot
      poseImg = canvas.toDataURL('image/jpg');
    }
    let formData = new FormData();
    formData.append("filedata", poseImg);
    formData.append("pose_name", poseData[currentPosNum].name);
    // const url = spinnerInfo=="EDGE"?apiPaths?.upload_pose:apiPaths.
    let res = null;
    
    if (spinnerInfo == 'EDGE' || envPlatform == 'EDGE') {

      if (isPending) return; // If already loading, don't send another request
      setIsPending(true);
      try {
        // Perform your POST request
        res = await postApi(formData, apiPaths?.upload_pose);
      } catch (error) {
        // Handle errors
      } finally {
        setIsPending(false); // Reset loading status regardless of success or failure
      }

    } else {

      if (isPending) return; // If already loading, don't send another request
      setIsPending(true);
      try {
        // Perform your POST request
        res = await cloudpostApi(formData, apiPaths?.upload_pose);
      } catch (error) {
        // Handle errors
      } finally {
        setIsPending(false); // Reset loading status regardless of success or failure
      }

    }

    if (!sendRequest.current) return;
    if (res.data.detection) {
      console.log("detaction stated", sendRequest.current, currentPosNum, poseImg);

      if (currentPosNum < poseData.length) {
        changePose("detect");
        let formData2 = new FormData();
        formData2.append("filedata", poseImg);
        formData2.append("tx_id", anonymousUserId);

        if (userImage == true) {
          postApiUrl2(formData2, apiPaths.savePictures);
        }
      }
    }
  }
  function changePose(type) {
    sendRequest.current = false;
    // if(type=="detect"){
    //   const response1 = await postApiGcp(formData, apiPaths?.savePictures);
    // }
    if (type == "skip") {
      setPoseTime((prev) => {
        return prev.map((item, index) => {
          if (index == currentPosNum) {
            let timeTaken = 0;
            return { ...item, isDisabled: false, time: timeTaken, skiped: true };
          }
          return item;
        })

      });
    } else {
      setPoseTime((prev) => {
        return prev.map((item, index) => {
          if (index == currentPosNum) {
            let timeTaken = timeData.current.timeReached - timeData.current.remainingTime;
            return { ...item, isDisabled: false, time: timeTaken };
          }
          return item;
        })
      });
    }
    if (currentPosNum < poseData.length - 1) {
      console.log(type, 'this is type')
      setShowLogo(true);
      setTimeout(() => {
        skipImage();
        setShowLogo(false);

      }, 1000)
    } else {
      allPoseDone();
    }

  }
  function skipImage() {
    console.log(poseData[currentPosNum], 'this is current', poseTimes[currentPosNum]);
    if (currentPosNum < poseData.length - 1) {
      timeData.current.timeReached = timeData.current.remainingTime;
      setCurrentPoseNum((prev) => {
        return prev + 1;
      })
    }
  }
  function allPoseDone() {
    console.log("allPoseDone");
    setShowPopup(true);
  }

  function closePage() {
    navigate("/EventPhoto")
  }

  return (
    <div className="pose_container">
      <Header clickCloseBtn={closePage} />
      <div className="pose_box">
        <div className="pose_image">
          <img src={poseData[currentPosNum].img} />
          {showLogo &&
            <div className="pose_img_logo_loader">
              <ReadyPoseLogo />
            </div>
          }
        </div>
        <div className="pose_cam">
          {camMode == "rtsp" ? <video
            ref={playerRef}
            // url={camUrl}
            controls
            // playing
            width="100%"
            height="100%"
            crossOrigin="anonymous"
            autoPlay
            muted
          /> :
            <Webcam
              audio={false}
              screenshotFormat="image/jpeg"
              ref={webcamRef}
              videoConstraints={{
                facingMode: facingMode
              }}
            />
          }
          <div className="skip_btn_box">
            <button className="cusButton" onClick={() => { changePose("skip") }} disabled={showLogo}>Skip</button>
          </div>
        </div>
      </div>

      <div className="pose_bottom_container">
        <div className="pose_timer_box">
          <div>
            <CountdownCircleTimer
              isPlaying={!showPopup}
              duration={60}
              colors={['#0B57D0', '#fff']}
              colorsTime={[60, 0]}
              size="80"
              onComplete={allPoseDone}
              onUpdate={setPostData}

            >
              {({ remainingTime }) => <div className="timer-text">{remainingTime}</div>}
            </CountdownCircleTimer>
          </div>
        </div>

        <div className="pose_count_box">
          Pose {currentPosNum + 1} of 10
        </div>
        <div className="pose_list_box">
          <div>
            <div className="post_list_time_head">POSE OFF TIME</div>
            <div className="post_list_time_text"><span>Performance on:</span><span>{spinnerInfo}</span></div>
          </div>
          <div className="pose_time_container">
            {poseTimes.map((item) =>
              <div className="pose_time_box" >
                <div style={{
                  backgroundColor: item.isDisabled ? "#E6E6E6" : (item.time === 0 ? '#EC4058' : '#2ED573'),
                  color: item.isDisabled ? "#9E9E9E" : "#FFFFFF",
                }}
                >

                  {!item.isDisabled && !item.skiped && `${item.time}s`}
                </div>
                <div>{item.text}</div>
              </div>
            )}
          </div>
          <canvas ref={canvasRef} style={{ display: 'none' }} crossOrigin="anonymous" id="testCanvasId" width="510px" height="382px" />
        </div>
      </div>
      {showPopup &&
        <NamePopup data={poseTimes} plateform={spinnerInfo} setUserInfo={setUserInfo} defaultName={anonymousUserInfo} txtId={anonymousUserId} />
      }
    </div>
  );
};


function ReadyPoseLogo({ size = "176px", text = "Get Ready for New Pose" }) {

  return (
    <div className="ready_pose_logo" style={{ width: size, height: size }}>
      <div className="rotate_box">
        <img className="" src={countDownImg} />
      </div>
      <div className="rotate_text">{text}</div>
    </div>
  )
}

function NamePopup({ closePopup, defaultName = "Anomymous0627", data, txtId, plateform, setUserInfo }) {
  const [name, setName] = useState("");
  const [updating, setUpdating] = useState(false);
  const navigate = useNavigate();
  function sendPoseData() {
    setUpdating(true);
    const sendData = {
      username: name != "" ? name : defaultName,
      platform: plateform,
      tx_id: txtId
    };
    let totalScore = 0;
    for (let item of data) {
      sendData[item.sendText] = (!item.isDisabled && !item.skiped) ? `${item.time}` : "";
      totalScore += item.time;
    }
    sendData["final_score"] = parseInt(`${totalScore}`);
    postApiUrl2(sendData, apiPaths.saveScore, "application/json")
      .then((res) => {
        let data = res.data;
        if (data.status == "success") {
          setUserInfo({
            name: data.player_scores?.username,
            txtId: data.player_scores?._id
          });
          navigate('/Leaderboard');
        }
      })
      .catch((err) => {
        setUpdating(false);
      })
  }
  return (
    <div className="name_popup">
      <div className="popup_inner_box">
        <div className="popup_header">
          <div>Enter Your Name</div>
        </div>
        <div className="popup_body">
          <div className="input_box">
            <input value={name} onChange={(e) => { setName(e.target.value) }} placeholder={defaultName}></input>
          </div>
          <div className="popup_btn_box">
            <button className="cusButton" disabled={updating} onClick={() => { sendPoseData("with") }}>{updating ? "Saving.." : "Save And Continue"}</button>
          </div>
        </div>
      </div>
    </div>
  )
}
export default Pose;


