import React from "react";
import { useState, useEffect } from "react";
import SettingHeader from "../../common/header/SettingHeader";
import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { mainContext } from "../../store/Store";
import { apiPaths } from "../../service/apipaths/ApiPaths";
import { postApiUrl2, getApiUrl2 } from "../../service/service/Service";

export default function Setting({ handleClose }) {

    const { setCameraMode, setEnvPlatform, setCamUrl, setSpinnerInfo } = useContext(mainContext);
    const navigate = useNavigate();
    const [updating, setUpdating] = useState(0);
    const [selectedOption, setSelectedOption] = useState('');
    const [selectedOptionPlatform, setSelectedOptionPlatform] = useState('');
    const [selectedOption2, setSelectedOption2] = useState('');
    const [selectedOptionDD, setSelectedOptionDD] = useState('');
    const [inputValue, setInputValue] = useState('');
    const [isDisabled,setIsDisabled] = useState(true);
    const [isSelected,setIsSelected] = useState(true);

    const handleChangeInput = (event) => {
        setIsDisabled(false);
        setInputValue(event.target.value);
    };

    const handleChangeD1 = (event) => {
        setIsDisabled(false);
        setSelectedOption(event.target.value);
        if(event.target.value == "EDGE/CLOUD"){
            setSelectedOptionPlatform("CLOUD");
        }
        else{   
            setSelectedOptionPlatform("EDGE");
        }
    };

    const handleChangeD2 = (event) => {
        setIsDisabled(false);
        setSelectedOption2(event.target.value);
        if (event.target.value == "WEB-CAM") {
            setSelectedOptionDD("user");
        }
        else if (event.target.value == "USB-CAM") {
            setSelectedOptionDD("enviroment");
        }
        else if (event.target.value == "RTSP-CAM") {
            setSelectedOptionDD("rtsp");
        }
        else {
            setSelectedOptionDD("");
        }
    };

    const handleSaveSetting = () => {
        setUpdating(1);
        const sendData = {
            "game_mode": selectedOptionPlatform ? selectedOptionPlatform : "",
            "cam_mode": selectedOptionDD ? selectedOptionDD : "",
            "cam_url": inputValue ? inputValue : ""
        }
        postApiUrl2(sendData, apiPaths.changeSettings, "application/json")
            .then((res) => {
                console.log("res :",res.data.cam_url);
                setEnvPlatform(res.data.game_mode);
                setSpinnerInfo(res.data.game_mode);
                setCameraMode(res.data.cam_mode);
                setCamUrl(res.data.cam_url);
                setTimeout(() => {
                    setUpdating(2);
                    setTimeout(() => {
                        navigate('/EventPhoto');
                    }, 1000)
                }, 1000)
            })
            .catch((err) => {
                setUpdating(0);
            })
    }

    const handleCross = () => {
        navigate('/EventPhoto')
    }

    const handleCancel = () => {
        navigate('/EventPhoto')
    }

    const getSettings = async () => {
        const response = await getApiUrl2(apiPaths?.getSettings);
        const data = response?.data[0];
        if(data.game_mode != "" || data.cam_mode != "" || data.cam_url != ""){
            setIsDisabled(false);
        }
        if(data.game_mode == "" && data.cam_mode == "" && data.cam_url == ""){
            setIsSelected(false);
        }
        if(data.game_mode == "EDGE"){
            setSelectedOption("EDGE ONLY");
            setSelectedOptionPlatform(data.game_mode);
        }
        else if(data.game_mode == "CLOUD"){
            setSelectedOption("EDGE/CLOUD");
            setSelectedOptionPlatform(data.game_mode);
        }
        else {
            setSelectedOption("");
            setSelectedOptionPlatform("");
        }

        if(data.cam_mode == "user"){
            setSelectedOption2("WEB-CAM");
            setSelectedOptionDD(data.cam_mode);
        }
        else if(data.cam_mode == "enviroment"){
            setSelectedOption2("USB-CAM");
            setSelectedOptionDD(data.cam_mode);
        }
        else if(data.cam_mode == "rtsp"){
            setSelectedOption2("RTSP-CAM");
            setSelectedOptionDD(data.cam_mode);
        }
        else {
            setSelectedOption2("");
            setSelectedOptionDD("");
        }

        if(data.cam_url){
            setInputValue(data.cam_url);
        }
    }

    useEffect(() => {
        getSettings();
      }, []);

    return (
        <div>
            <SettingHeader handleClose={handleCross} />
            <div className="setting_top_content">
                <p>change game settings</p>
            </div>
            <div className="bottom_content_parent">
                <div className="bottom_first">
                    Game Mode:
                </div>
                <div className="custom-dropdown">
                    <select value={selectedOption} onChange={handleChangeD1}>
                        <option selected={isSelected}>EDGE/CLOUD</option>
                        <option>EDGE ONLY</option>
                    </select>
                </div>
            </div>
            <div className="bottom_content_parent">
                <div className="bottom_first">
                    Cam Mode:
                </div>
                <div className="custom-dropdown">
                    <select value={selectedOption2} onChange={handleChangeD2}>
                        <option selected={isSelected}>WEB-CAM</option>
                        <option>USB-CAM</option>
                        <option>RTSP-CAM</option>
                    </select>
                </div>
            </div>
            <div className="bottom_content_parent">
                <div className="bottom_first">
                    Cam URL:
                </div>
                <div style={{ paddingRight: "20px" }}>
                    <input
                        type="text"
                        value={inputValue}
                        onChange={handleChangeInput}
                        className="custom-input"
                        placeholder="Enter url here"
                    />
                </div>
            </div>
            <div className="leader__board-button-container2">
                <button className="leader_board-button2" onClick={handleCancel}>CANCEL</button>
                <button className="play_again-button2" disabled={isDisabled} onClick={handleSaveSetting}>{updating == 1 ? "SAVING.." : updating == 2 ? "SETTING SAVED!" : "SAVE SETTING"}</button>
            </div>
        </div>
    )
}