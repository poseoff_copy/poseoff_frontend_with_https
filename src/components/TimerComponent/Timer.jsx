import React, { useEffect, useState } from "react";
import { CountdownCircleTimer } from 'react-countdown-circle-timer';
import "./timer.css";

const customTextStyle = {
  fontFamily: 'robotoCondensed',
  fontSize: '24px', // Adjust the font size as needed
  fontWeight: 'bold', // Adjust the font weight as needed
  color: "blue"
};

const UrgeWithPleasureComponent = ({timeCompleteHandler}) => (
  
  <div className='timerCl'>
    <CountdownCircleTimer
      isPlaying
      duration={300}
      colors={['#2699fb', '#2699fb', '#2699fb', '#2699fb']}
      colorsTime={[7, 5, 2, 0]}
      size="100"
      onComplete={timeCompleteHandler}
      style={customTextStyle}
    >
      {({ remainingTime }) => remainingTime}
    </CountdownCircleTimer>
  </div>
);

export default UrgeWithPleasureComponent;