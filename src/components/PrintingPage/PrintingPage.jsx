import React, { useEffect, useState, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
// import "../waitlist/WaitList.css";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import "./printing.css";
import Search from "../../common/search/Search";
import { getApi, getApiUrl2, getApiUrl3 } from "../../service/service/Service";
import { apiPaths } from "../../service/apipaths/ApiPaths";
import { mainContext } from "../../store/Store";
import btnClose from "../../assets/btn-close.png";

const PrintingPage = () => {
  const [value, setValue] = useState(0);

  const { setGalleryUserName, setGalleryUserEmail } = useContext(mainContext);

  const navigate = useNavigate();
  const [playersList, setPLayersList] = useState([
    { name: "Doe John", phNO: "9875647498", email: "john@gmail.com" },
    { name: "Bncdjncj", phNO: "9875647498", email: "john@gmail.com" },
    { name: "Johny Dep", phNO: "9875647498", email: "john@gmail.com" },
    { name: "Acdhcb", phNO: "9875647498", email: "john@gmail.com" },
  ]);

  const [copiedPLayerList, setCopiedPLayerLIst] = useState([
    { name: "Doe John", phNO: "9875647498", email: "john@gmail.com" },
    { name: "Bncdjncj", phNO: "9875647498", email: "john@gmail.com" },
    { name: "Johny Dep", phNO: "9875647498", email: "john@gmail.com" },
    { name: "Acdhcb", phNO: "9875647498", email: "john@gmail.com" },
  ]);

  useEffect(() => {
    const getPrintList = async () => {
      const data = await getApiUrl2(apiPaths?.print);
      setPLayersList(data?.data);
      setCopiedPLayerLIst(data?.data);
    };
    getPrintList();
  }, []);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const printingPageHandler = (e) => {
    const newCopiedPLayerList = copiedPLayerList.filter((item) =>
      item?.username?.toLowerCase()?.includes(e.target.value.toLowerCase())
    );
    setPLayersList(newCopiedPLayerList);
  };

  const galleryOnChangeHandler = async (username, email, userId) => {
    setGalleryUserName(username);
    setGalleryUserEmail(email);

    const response = await getApiUrl2(`${apiPaths?.showPicture}?tx_id=${userId}`)
    navigate("/GalleryPage", {
      state: {
        username,
        email,
        userId
      },
    });
  };

  return (
    <>
      <div className="parentContainerList">
        <div className="printing__header">
          <p className="printing__header-heading">Print</p>

          {/* <span className="childContainer2"> */}
          <img
            style={{ width: "4%" }}
            src={btnClose}
            onClick={() => navigate(-1)}
          />
        </div>
        {/* </span> */}
        {/* <div className="tabs__container"> */}
        <p className="printing__edge_subheading">
          Print photos taken during Pose Off game
        </p>
        {/* </div> */}
      </div>
      {/* <CustomTabPanel value={value} index={0}> */}
      <div className="printing__parent-container">
        <div className="waitlist__subheader-container">
          <p className="total__palyers-heading mb-0">
            Total ({playersList?.length}) Players
          </p>
          <Search onChangeHandler={printingPageHandler} />
        </div>

        {Array.isArray(playersList) && playersList?.map((item, idx) => {
          return (
            <div key={idx} className="parent__player-container">
              <div className="playerlist__right-container">
                <img src="src/assets/ic-user.svg" alt="" />
                <div className="name__ph__no-container">
                  <span className="playerlist__name">{item?.username}</span>
                  <span className="playerlist__phno">{item?.email}</span>
                </div>
              </div>
              <div>
                {/* <Link to="/gallery"> */}
                <button
                  className="playerlist__button"
                  onClick={() =>
                    galleryOnChangeHandler(item?.username, item?.email, item?._id)
                  }
                >
                  GALLERY
                </button>
                {/* </Link> */}
              </div>
            </div>
          );
        })}
      </div>
      {/* <div class="circle-container">
        <div class="circle">OR</div>
      </div>
      <section>
        <div className="heading__container">
          <p className="search__section-heading">
            SEARCH FOR <br />
            EXISTING PLAYERS
          </p>
        </div>
        <div className="form__contaienr">
          <form>
            <input
              className="input__Search"
              type="search"
              placeholder="Search by name, email or ID"
            />
            <button className="printscreen__button">SEARCH</button>
          </form>
        </div>
      </section> */}

      {/* </CustomTabPanel> */}
      <div className="bottomFooterPrint">
        <p id="bottomMarkText">
          Powered by <b>Google</b>
        </p>
      </div>
    </>
  );
};

export default PrintingPage;

function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
