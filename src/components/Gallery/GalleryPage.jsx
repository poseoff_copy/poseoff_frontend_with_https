import React, { useState, useContext } from "react";
// import "../waitlist/WaitList.css";
import "./GalleryPage.css";
import GalleryImage from "../../assets/thumb-gallery-1.png";
import { Link, useNavigate, useLocation } from "react-router-dom";
import thumbSelectedOverlay from "../../assets/thumb-selected-overlay.png";
import arraowBack from "../../assets/arrow_back.svg";
import { mainContext } from "../../store/Store";
import { useEffect } from "react";
import { getApiUrl2,getApiUrl3 } from "../../service/service/Service";
import { apiPaths } from "../../service/apipaths/ApiPaths";
import { url } from "../../service/url/url";
import CloseWhite from "../../assets/ic-close-white.svg";
import btnClose from "../../assets/btn-close.png";
import printJS from "print-js";


const GalleryPage = () => {
  const [value, setValue] = useState(0);

  const { galleryUserName, } = useState(mainContext);

  const [printModal, setPrintModal] = useState(false);
  const [playersList, setPLayersList] = useState([
    { name: "John Doe", phNO: "9875647498" },
    { name: "John Doe", phNO: "9875647498" },
    { name: "John Doe", phNO: "9875647498" },
    { name: "John Doe", phNO: "9875647498" },
  ]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  // useEffect(() => {
  //   const id = setInterval(() => setNum(p => p + 1), 2000);
  //   console.log(id);
  //   return () => clearInterval(id);
  // }, []);
  const { anonymousUserId } = useContext(mainContext);
  const navigate = useNavigate();
  const { state } = useLocation();
  const [galleryData, setGalleryData] = useState([
    { id: 1, src: GalleryImage, isSelected: false },
    { id: 2, src: GalleryImage, isSelected: false },
    { id: 3, src: GalleryImage, isSelected: false },
    { id: 4, src: GalleryImage, isSelected: false },
    { id: 5, src: GalleryImage, isSelected: false },
    { id: 6, src: GalleryImage, isSelected: false },
  ]);

  const getPhotos = async () => {
    const response = await getApiUrl2(`${apiPaths?.showPicture}?tx_id=${state?.userId}`)
    const modifiedData = response?.data?.map((item, idx) => {
      return {
        ...item,
        src: `${url?.url2}/${item?.img_link}`,
        isSelected: false,
        id: idx,
      };
    });
    setGalleryData(modifiedData);
  };

  useEffect(() => {
   
    getPhotos();
  }, []);
  const selectedImage = (idx) => {
    const selectedImages = galleryData.map((item) => {
      if (item.id === idx) {
        return {
          ...item,
          isSelected: !item.isSelected,
        };
      } else return item;
    });
    setGalleryData(selectedImages);
  };

  const handleCheckBoxChange = (e) => {
    let selectedImages;
    if (e?.target?.checked) {
      selectedImages = galleryData?.map((item) => {
        return {
          ...item,
          isSelected: true,
        };
      });
    } else {
      selectedImages = galleryData?.map((item) => {
        return {
          ...item,
          isSelected: false,
        };
      });
    }
    setGalleryData(selectedImages);
  };

  return (
    <>
      <div className="parentContainerList">
        <div className="printing__header">
          <div className="d-flex">
            <img
              className="gallery__arrow-back"
              onClick={() => navigate(-1)}
              src={arraowBack}
            />

            <p className="printing__header-heading">Gallery</p>
          </div>

          {/* <span className="childContainer2"> */}

          <img
            style={{width:"4%",height: "4%"}}
            src={btnClose}
            onClick={() => navigate(-1)}
          />
        </div>
        {/* </span> */}
        {/* <div className="tabs__container"> */}
        {/* <p className="printing__edge_subheading">
          Get the printed copies of the GDCE photos
        </p> */}
          {/* <div id="test">
           
          </div> */}
        <div className="gallery__header-parent-container">
          <div className="gallery__header-right-container">
            <span className="gallery__player-name">
              {state?.username || "john Doe"}
            </span>
            <span className="gallery__player-ph-no">{state?.email}</span>
          </div>
          <div className="gallery__checkbox-container">
            <input
              className="gallery__header-checkbox"
              onChange={handleCheckBoxChange}
              type="checkbox"
              checked={galleryData?.every((item) => item?.isSelected)}
            />
            <span className="gallery__checkbox-label">Select All</span>
          </div>
        </div>
        {/* </div> */}
      </div>
      <section className="gallery__img-container">
        <div className="d-flex flex-wrap">
          {galleryData?.map((item, idx) => {
            return (
              <div className="img_parent-container" key={idx}>
                <div className="gallery-parent">
                  <div className="gallery__check-icon">
                    {item.isSelected && (
                      <img
                        src={thumbSelectedOverlay}
                        onClick={() => selectedImage(item.id)}
                      />
                    )}
                  </div>
                  <img
                    className="img-fluid"
                    onClick={() => selectedImage(item.id)}
                    src={item?.src}
                  />
                </div>
              </div>
            );
          })}
        </div>
        {/* <button
          disabled={
            galleryData?.filter((item) => item.isSelected).length > 0
              ? false
              : true
          }
          className="gallery__button"
          onClick={() => setPrintModal(true)}
        >
          PRINT
        </button> */}
      </section>
      <button 
       disabled={
        galleryData?.filter((item) => item.isSelected).length > 0
          ? false
          : true
      }
      className="gallery__button" 
      onClick={() => printJS({
        printable: galleryData?.filter(item => item?.isSelected)?.map(item => item?.src),
        type: 'image',
        // header: 'Multiple Images',
        imageStyle: 'width:40%;margin-bottom:20px;'
      })}>
          PRINT
        </button>
      {/* <ReactToPrintContent
        galleryData={galleryData.filter((item) => item.isSelected)}
        selectedImage={selectedImage}
        printModal={printModal}
        setPrintModal={setPrintModal}
      /> */}

      {/* <CustomTabPanel value={value} index={0}> */}

      {/* </CustomTabPanel> */}
    </>
  );
};

export default GalleryPage;
