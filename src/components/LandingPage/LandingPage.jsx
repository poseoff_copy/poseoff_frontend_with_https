import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import splashX from "../../assets/splogo.png";

const LandingPage = () => {
  const navigate = useNavigate();

  useEffect(() => {
    setTimeout(function () {
      navigate("/EventPhoto");
    }, 5000);
  }, []);

  return (
      <div className="imageContainer">
        <div>
          <img className="image-auto" src={splashX} />
        </div>
      </div>
  );
};

export default LandingPage;
