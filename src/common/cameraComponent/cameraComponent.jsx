import React, { useRef, useEffect,useState } from "react";
import Webcam from "react-webcam";
import "./cameracomp.css";
import { postApi } from "../../service/service/Service";
import { apiPaths } from "../../service/apipaths/ApiPaths";

const CameraComponent = () => {
  const webcamRef = useRef(null);

  const [detection,setDetection] = useState(false);

  
  const capture = async () => {
    const imageSrc = webcamRef.current.getScreenshot();
    const sendData = {
      pose_name:'Pose1',
      filedata:imageSrc
    }
    const formData = new FormData();
    
    formData.append('filedata', imageSrc);
    formData.append('pose_name', 'Pose2');
    
    const response = await postApi(formData, apiPaths?.upload_pose);
    setDetection(response?.data?.detection)
    // setDetection(response?.)

  };

  useEffect(() => {
    const id = setInterval(capture, 1000);
    return () => clearInterval(id);
  }, []);

  return (
    <>
    {/* {detection && alert('your pose is right')} */}
    <Webcam
      audio={false}
      // height={160}
      ref={webcamRef}
      screenshotFormat="image/jpeg"
      // width={160}
      // width={100}
    />
    </>
  );
};

export default CameraComponent;
