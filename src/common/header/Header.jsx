import React from "react";
import { Link } from "react-router-dom";

import logo from "../../assets/Pose-Off/Pose-Off.png";
import btnInstruction from "../../assets/btn-instruction/btn-instruction.png";
import btnLeaderBoard from "../../assets/btn-leaderboard/btn-leaderboard.png";
import btnClose from "../../assets/btn-close/btn-close.png";
import "./header.css";

const Header = ({clickCloseBtn}) => {
  return (
    <div className="header__container">
      <div className="poseOff__logo">
        <img  src={logo} />
      </div>
      <div className="header__right-container">
        {/* <div>
          <img src={btnInstruction} />
        </div>
        <Link to="/LeaderBoardOne">
          {" "}
          <img src={btnLeaderBoard} />
        </Link> */}
        <div onClick={clickCloseBtn}>
          <img src={btnClose} />
        </div>
      </div>
    </div>
  );
};

export default Header;
