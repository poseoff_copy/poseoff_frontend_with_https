import React from "react";
import btnClose from "../../assets/btn-close.png";
import PoseOff from "../../assets/Group 1084.png";
import PoseTxt from "../../assets/Pose-Off.png";
export default function ResultHeader({handleClose}) {

  return(
    <div className="cloud-hdr_result">
        <div className="itemContainerHeader_result">
          <div>
          <img src={PoseTxt}></img>
          </div>
          <div className="close-btn_result">
            <img src={btnClose} onClick={handleClose}></img>
          </div>
        </div>
        <div className="tidy-tPS_result">
          <img src={PoseOff}></img>
        </div>
      </div>
  )
}