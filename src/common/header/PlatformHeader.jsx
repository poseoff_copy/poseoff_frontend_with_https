import React from "react";
import btnClose from "../../assets/btn-close.png";
import PoseOff from "../../assets/thumb-TT-selected.png";
import PoseTxt from "../../assets/Pose-Off.png";
export default function PlatformHeader({handleClose}) {

  return(
    <div className="cloud-hdr">
        <div className="itemContainerHeader">
          <div style={{visibility:"hidden"}}>
            <div className="select-text">Select</div>
            <div className="plateform-text">Test your flexibility: strike a pose or laugh trying!</div>
          </div>
          <div className="close-btn">
            <img src={btnClose} onClick={handleClose}></img>
          </div>
        </div>
        <div className="tidy-tPS">
          <img src={PoseOff}></img>
          <div className="tidy-tPS-posetxt">
            <img src={PoseTxt}></img>
          </div>
        </div>
      </div>
  )
}