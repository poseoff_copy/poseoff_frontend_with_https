import React from "react";
import searchGrey from "../../assets/ic-search-gray.svg";

const Search = ({ onChangeHandler }) => {
  return (
    <div className="wailtlist__search-container">
      <input
        className="input__search"
        type="search"
        placeholder="search"
        onChange={(e) => onChangeHandler(e)}
      />
      <img className="wailtlist__search-input-icon" src={searchGrey} />
    </div>
  );
};

export default Search;
